package com.test.logitech.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.logitech.R;
import com.test.logitech.model.entity.Datum;
import com.test.logitech.view.viewholder.DataViewHolder;

import java.util.ArrayList;
import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<DataViewHolder> {

    private List<Datum> dataList = new ArrayList<>();
    private Context context;

    public DataAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<Datum> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    private Datum getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_movie_list, parent, false);
        return new DataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        final Datum data = getItem(position);
        bindData(holder, data);
    }

    private void bindData(DataViewHolder holder, final Datum data) {
        holder.txtName.setText(data.name != null ? data.name : "");
        holder.txtYear.setText(data.year+"");
        holder.txtPhantomValue.setText(data.pantoneValue != null ? data.pantoneValue : "");
        int color = Color.parseColor(data.color);
        holder.linRoot.setBackgroundColor(color);


    }

}
