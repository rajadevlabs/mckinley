package com.test.logitech.view.viewinterface;

public interface LoginView {

    void showLoading();

    void hideLoading();

    void showError(String errorMessage);

    void showHomeScreen();

    void showNetworkError();
}