package com.test.logitech.view.viewholder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.test.logitech.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DataViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.view_parent)
    public CardView parentView;
    @BindView(R.id.lin_root)
    public LinearLayout linRoot;
    @BindView(R.id.txt_name)
    public TextView txtName;
    @BindView(R.id.txt_year)
    public TextView txtYear;
    @BindView(R.id.txt_phantom_value)
    public TextView txtPhantomValue;


    public DataViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}