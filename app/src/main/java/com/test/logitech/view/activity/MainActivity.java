package com.test.logitech.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.test.logitech.R;
import com.test.logitech.model.entity.Datum;
import com.test.logitech.presenter.HomeScreenPresenterImpl;
import com.test.logitech.presenter.presenterinterface.HomeScreenPresenter;
import com.test.logitech.utils.DialogUtils;
import com.test.logitech.utils.EqualSpacingItemDecoration;
import com.test.logitech.view.adapter.DataAdapter;
import com.test.logitech.view.viewinterface.HomeScreenView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements HomeScreenView {

    @BindView(R.id.list_view)
    RecyclerView movieListView;

    private HomeScreenPresenter presenter;
    private DataAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupLayout();

        presenter = new HomeScreenPresenterImpl();
        presenter.attachView(this);
        presenter.getData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void showLoading() {
        DialogUtils.showProgressDialog(this);
    }

    @Override
    public void hideLoading() {
        DialogUtils.hideProgressDialog();
    }

    @Override
    public void showError(String errorMessage) {
        DialogUtils.showErrorMessage(this, errorMessage);
    }

    @Override
    public void setData(List<Datum> data) {
        adapter.setData(data);
    }

    private void setupLayout() {
        adapter = new DataAdapter(this);
        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        movieListView.setLayoutManager(manager);
        movieListView.setAdapter(adapter);
        movieListView.addItemDecoration(new EqualSpacingItemDecoration(10));




    }
}
