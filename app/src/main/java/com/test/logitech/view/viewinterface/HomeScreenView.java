package com.test.logitech.view.viewinterface;

import android.graphics.Movie;

import com.test.logitech.model.entity.Datum;

import java.util.List;

public interface HomeScreenView {

    void showLoading();

    void hideLoading();

    void showError(String errorMessage);

    void setData(List<Datum> data);
}
