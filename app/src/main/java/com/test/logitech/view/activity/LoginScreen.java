package com.test.logitech.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.test.logitech.R;
import com.test.logitech.presenter.LoginPresenterImpl;
import com.test.logitech.presenter.presenterinterface.LoginPresenter;
import com.test.logitech.utils.DialogUtils;
import com.test.logitech.view.viewinterface.LoginView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginScreen extends AppCompatActivity implements LoginView
{


    @BindView(R.id.ed_username)
    EditText edUserName;
    @BindView(R.id.ed_password)
    EditText edPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;

    private LoginPresenter presenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        presenter = new LoginPresenterImpl();
        presenter.attachView(this);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.signing(edUserName.getText().toString().trim(), edPassword.getText().toString().trim());
            }
        });
    }


    @Override
    public void showLoading() {
        DialogUtils.showProgressDialog(this);
    }

    @Override
    public void hideLoading() {
        DialogUtils.hideProgressDialog();
    }


    @Override
    public void showError(String errorMessage) {
        DialogUtils.showErrorMessage(this, errorMessage);
    }

    @Override
    public void showHomeScreen() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showNetworkError() {
        DialogUtils.showErrorMessage(this, getString(R.string.error_internet));
    }
}
