package com.test.logitech.model.api;

import com.test.logitech.model.entity.LoginBody;
import com.test.logitech.model.entity.SigningBody;
import com.test.logitech.model.entity.Test;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiService {

    /*
     * Add as many as you can API's for fetching data. collectively here
     * and get response on required screen/Class.
     * */

    @GET("api/login")
    Call<Test> getDetails();

    @POST("api/login")
    Call<SigningBody> signing(@Body LoginBody body);
}
