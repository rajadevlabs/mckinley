package com.test.logitech.model.entity;

import com.google.gson.annotations.SerializedName;

public class LoginBody {
    @SerializedName("email")
    public String email;
    @SerializedName("password")
    public String password;
}
