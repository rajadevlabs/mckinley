package com.test.logitech.model.entity;

import com.google.gson.annotations.SerializedName;

public class SigningBody {
    @SerializedName("id")
    public String id;
    @SerializedName("createdAt")
    public String createAt;
}
