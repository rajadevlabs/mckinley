package com.test.logitech.presenter;

import com.test.logitech.AppInstance;
import com.test.logitech.R;
import com.test.logitech.model.entity.LoginBody;
import com.test.logitech.model.entity.SigningBody;
import com.test.logitech.presenter.presenterinterface.LoginPresenter;
import com.test.logitech.view.viewinterface.LoginView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenterImpl extends BasePresenter<LoginView> implements LoginPresenter {

    @Override
    public void signing(final String email, final String password) {
        if (view == null)
            return;
        if (!isValidEmail(email)) {
            view.showError(AppInstance.getInstance().getString(R.string.error_email));
            return;
        }

        if (!isNetworkAvailable()){
            view.showNetworkError();
            return;
        }
        view.showLoading();

        LoginBody loginBody = new LoginBody();
        loginBody.email = email;
        loginBody.password = password;

        /*
        *
        * After many attempt Login is not successful due to [Not user found]
        * I try with [@FormEncoded {username, password}],[@Body {LoginBody}] and [@QueryMap {username, password}] all three
        * way the result is bad request.
        *
        * */

        apiFacade.getApiService().signing(loginBody).enqueue(new Callback<SigningBody>() {
            @Override
            public void onResponse(Call<SigningBody> call, Response<SigningBody> response) {
                if (view == null)
                    return;
                view.hideLoading();
                if (!response.isSuccessful())
                    view.showHomeScreen();
                else
                    view.showError(AppInstance.getInstance().getString(R.string.unknown_error));

            }

            @Override
            public void onFailure(Call<SigningBody> call, Throwable t) {

            }
        });
    }

    public static boolean isValidEmail(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        Pattern p = Pattern.compile(ePattern);
        Matcher m = p.matcher(email);
        return m.matches();
    }
}