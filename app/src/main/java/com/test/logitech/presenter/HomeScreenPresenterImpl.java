package com.test.logitech.presenter;

import com.test.logitech.AppInstance;
import com.test.logitech.R;
import com.test.logitech.model.entity.Test;
import com.test.logitech.presenter.presenterinterface.HomeScreenPresenter;
import com.test.logitech.view.viewinterface.HomeScreenView;

import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeScreenPresenterImpl extends BasePresenter<HomeScreenView> implements HomeScreenPresenter {

    @Override
    public void getData() {

        if (view == null)
            return;

        if (!isNetworkAvailable()) {
            view.showError(AppInstance.getInstance().getString(R.string.error_internet));
            return;
        }

        view.showLoading();

        apiFacade.getApiService().getDetails().enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
                if (view == null)
                    return;
                view.hideLoading();
                Test test = response.body();
                if (response.isSuccessful() && test != null) {
                    view.setData(test.data);
                }
            }

            @Override
            public void onFailure(Call<Test> call, Throwable t) {

            }
        });
    }
}
