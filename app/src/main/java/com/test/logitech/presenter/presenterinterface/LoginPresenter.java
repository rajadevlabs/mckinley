package com.test.logitech.presenter.presenterinterface;

import com.test.logitech.view.viewinterface.LoginView;

public interface LoginPresenter {

    void attachView(LoginView view);

    void detachView();

    void signing(String username, String password);
}